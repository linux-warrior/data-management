CREATE FUNCTION thread_update_columns(v_thread_id integer) RETURNS void AS $$
-- Обновляет значения некоторых колонок у направления маршрута `v_thread_id`.
--
-- Определяет пункт отправления, пункт прибытия и время в пути, исходя из
-- остановок, связанных с заданным направлением маршрута.

DECLARE
    v_min_arrival_interval interval;
    v_max_arrival_interval interval;
    v_departure_point_id integer;
    v_arrival_point_id integer;
    v_trip_duration interval;

BEGIN
    -- Поскольку данная функция будет вызываться после каждого изменения в таблице "point",
    -- следующая команда заблокирует параллельные транзакции, которые попытаются отредактировать
    -- остановки, связанные с направлением маршрута `v_thread_id`.
    PERFORM thread.id FROM thread WHERE thread.id = v_thread_id FOR UPDATE;
    IF NOT FOUND THEN
        RETURN;
    END IF;

    SELECT MIN(point.arrival_interval), MAX(point.arrival_interval)
        INTO v_min_arrival_interval, v_max_arrival_interval
        FROM point WHERE point.thread_id = v_thread_id;
    SELECT point.id INTO v_departure_point_id
        FROM point WHERE (
            point.thread_id = v_thread_id AND
            point.arrival_interval = v_min_arrival_interval
        );
    SELECT point.id INTO v_arrival_point_id
        FROM point WHERE (
            point.thread_id = v_thread_id AND
            point.arrival_interval = v_max_arrival_interval
        );

    -- Если с направлением маршрута не связано ни одной остановки,
    -- максимальное и минимальное время прибытия будут NULL.
    IF v_min_arrival_interval IS NOT NULL AND v_max_arrival_interval IS NOT NULL THEN
        v_trip_duration := v_max_arrival_interval - v_min_arrival_interval;
    ELSE
        v_trip_duration := NULL;
    END IF;

    UPDATE thread SET
        departure_point_id = v_departure_point_id,
        arrival_point_id = v_arrival_point_id,
        trip_duration = v_trip_duration
        WHERE thread.id = v_thread_id;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION point_before_modify_statement() RETURNS trigger AS $$

BEGIN
    -- Создаем временную таблицу, в которую мы будем сохранять направления маршрутов,
    -- для которых произошли изменения в таблице остановок в результате выполнения
    -- SQL-команды, активировавшей триггер.
    CREATE TEMP TABLE t_thread_changed (
        thread_id integer NOT NULL PRIMARY KEY
    ) ON COMMIT DROP;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION point_after_modify_row() RETURNS trigger AS $$

DECLARE
    v_point RECORD;

BEGIN
    IF TG_OP IN ('INSERT', 'UPDATE') THEN
        v_point := NEW;
    ELSIF TG_OP = 'DELETE' THEN
        v_point := OLD;
    END IF;

    -- Сохраняем направление маршрута для последующего обновления,
    -- если оно еще не сохранялось при обработке других строк.

    -- TODO: возможно, проще и быстрее было бы не создавать первичный ключ на
    -- колонке "thread_id" и не использовать обработку исключений, а просто сделать
    -- SELECT DISTINCT в соответствующей триггерной функции.
    BEGIN
        INSERT INTO t_thread_changed (thread_id) VALUES (v_point.thread_id);
    EXCEPTION WHEN unique_violation THEN
        NULL;
    END;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION point_after_modify_statement() RETURNS trigger AS $$

DECLARE
    v_thread_id integer;

BEGIN
    -- Обновляем все направления маршрутов, для которых были изменения остановок.
    -- Направления маршрутов обрабатываем в порядке возрастания первичного ключа,
    -- чтобы избежать deadlocks.
    FOR v_thread_id IN
        SELECT t_thread_changed.thread_id FROM t_thread_changed ORDER BY t_thread_changed.thread_id
    LOOP
        PERFORM thread_update_columns(v_thread_id);
    END LOOP;
    -- Удаляем временную таблицу, созданную до этого в другой триггерной функции.
    DROP TABLE t_thread_changed;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;


-- Эти триггеры будут обновлять таблицу направлений маршрутов после внесения
-- изменений в таблицу остановок.
CREATE TRIGGER point_before_modify_statement BEFORE INSERT OR UPDATE OR DELETE ON point
    FOR EACH STATEMENT EXECUTE PROCEDURE point_before_modify_statement();
CREATE TRIGGER point_after_modify_row AFTER INSERT OR UPDATE OR DELETE ON point
    FOR EACH ROW EXECUTE PROCEDURE point_after_modify_row();
CREATE TRIGGER point_after_modify_statement AFTER INSERT OR UPDATE OR DELETE ON point
    FOR EACH STATEMENT EXECUTE PROCEDURE point_after_modify_statement();


CREATE FUNCTION thread_reverse(v_thread_id integer,
                               v_copy_trips boolean DEFAULT TRUE,
                               v_offset interval DEFAULT '0',
                               v_before boolean DEFAULT FALSE) RETURNS integer AS $$
-- Создает обратное направление маршрута для `v_thread_id`.
--
-- Копирует все остановки в обратном порядке, "инвертируя" интервал прибытия.
--
-- Параметры:
--     v_copy_trips: также будут скопированы связанные рейсы с помощью
--         функции trip_reverse().
--     v_offset, v_before: параметры, которые будут переданы в функцию
--         trip_reverse().
--
-- Возвращаемое значение:
--     "id" вновь созданного направления маршрута или NULL, если направление
--     `v_thread_id` не найдено.

DECLARE
    v_thread RECORD;
    v_min_arrival_interval interval;
    v_max_arrival_interval interval;
    v_new_thread_id integer;

BEGIN
    SELECT thread.id, thread.route_id INTO v_thread FROM thread WHERE thread.id = v_thread_id FOR SHARE;
    IF NOT FOUND THEN
        RETURN NULL;
    END IF;

    -- TODO: в целях денормализации можно добавить колонки "min_arrival_interval" и
    -- "max_arrival_interval" в таблицу "thread".
    SELECT MIN(point.arrival_interval), MAX(point.arrival_interval)
        INTO v_min_arrival_interval, v_max_arrival_interval
        FROM point WHERE point.thread_id = v_thread_id;

    INSERT INTO thread (route_id) VALUES (v_thread.route_id) RETURNING thread.id INTO v_new_thread_id;

    -- Если с направлением маршрута не связано ни одной остановки,
    -- максимальное и минимальное время прибытия будут NULL.
    IF v_min_arrival_interval IS NOT NULL AND v_max_arrival_interval IS NOT NULL THEN
        INSERT INTO point (thread_id, location_id, arrival_interval)
            SELECT
                v_new_thread_id,
                point.location_id,
                v_min_arrival_interval + v_max_arrival_interval - point.arrival_interval
            FROM point WHERE point.thread_id = v_thread_id
            ORDER BY point.arrival_interval DESC;
    END IF;

    IF v_copy_trips THEN
        PERFORM trip_reverse(trip.id, v_new_thread_id, v_offset := v_offset, v_before := v_before)
        FROM trip WHERE trip.thread_id = v_thread_id
        ORDER BY trip.id;
    END IF;

    RETURN v_new_thread_id;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION trip_reverse(v_trip_id integer,
                             v_dest_thread_id integer,
                             v_offset interval DEFAULT '0',
                             v_before boolean DEFAULT FALSE) RETURNS integer AS $$
-- Создает обратный рейс для `v_trip_id`.
--
-- Параметры:
--     v_dest_thread_id: направление маршрута, с которым будет связан обратный рейс.
--     v_offset: величина, на которую "departure_time" создаваемого обратного рейса
--         будет отличаться от "arrival_time" рейса `v_trip_id` (если v_before = False).
--     v_before: если v_before = True, тогда, наоборот, "arrival_time" создаваемого
--         обратного рейса будет меньше "departure_time" рейса `v_trip_id` на величину
--         `v_offset`.
--
-- Возвращаемое значение:
--     "id" созданного обратного рейса или NULL, если рейс `v_trip_id` не был найден.

DECLARE
    v_trip RECORD;
    v_new_departure_time time;
    v_new_arrival_time time;
    v_new_trip_id integer;

BEGIN
    SELECT
        trip.id,
        trip.departure_time,
        trip.arrival_time,
        trip.arrival_time - trip.departure_time AS trip_duration
        INTO v_trip FROM trip WHERE trip.id = v_trip_id;
    IF NOT FOUND THEN
        RETURN NULL;
    END IF;

    IF NOT v_before THEN
        v_new_departure_time := v_trip.arrival_time + v_offset;
        v_new_arrival_time := v_new_departure_time + v_trip.trip_duration;
    ELSE
        v_new_arrival_time := v_trip.departure_time - v_offset;
        v_new_departure_time := v_new_arrival_time - v_trip.trip_duration;
    END IF;

    INSERT INTO trip (thread_id, departure_time, arrival_time) VALUES
        (v_dest_thread_id, v_new_departure_time, v_new_arrival_time)
        RETURNING trip.id INTO v_new_trip_id;

    RETURN v_new_trip_id;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION trip_before_save_row() RETURNS trigger AS $$

DECLARE
    v_thread RECORD;

BEGIN
    IF NEW.arrival_time IS NULL THEN
        -- Автоматически определяем время прибытия, используя значение
        -- времени в пути из связанного направления маршрута.

        SELECT thread.id, thread.trip_duration INTO v_thread
            FROM thread WHERE thread.id = NEW.thread_id FOR SHARE;
        IF FOUND THEN
            NEW.arrival_time := NEW.departure_time + v_thread.trip_duration;
        END IF;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER trip_before_save_row BEFORE INSERT OR UPDATE ON trip
    FOR EACH ROW EXECUTE PROCEDURE trip_before_save_row();


CREATE FUNCTION thread_search(v_start_location_name text,
                              v_end_location_name text,
                              v_start_arrival_time time DEFAULT '00:00')
    RETURNS TABLE (
        thread_id integer,
        trip_id integer,
        start_point_id integer,
        start_point_arrival_interval interval,
        start_point_arrival_time time,
        end_point_id integer,
        end_point_arrival_interval interval,
        end_point_arrival_time time
    ) AS $$
-- Находит все направления маршрутов, проходящие через две заданные станции.
--
-- Таблица с результатами поиска будет отсортирована по возрастанию времени
-- прибытия на начальную станцию.
--
-- Параметры:
--     v_start_location_name: название начальной станции.
--     v_end_location_name: название конечной станции.
--     v_start_arrival_time: время прибытия на начальную станцию должно быть
--         больше или равно этой величине.

BEGIN
    RETURN QUERY WITH
        start_point AS (
            SELECT
                point.id,
                point.thread_id,
                point.arrival_interval,
                trip.id AS trip_id,
                (trip.departure_time + point.arrival_interval) AS arrival_time
            FROM point
                INNER JOIN location ON (point.location_id = location.id)
                INNER JOIN thread ON (point.thread_id = thread.id)
                INNER JOIN trip ON (thread.id = trip.thread_id)
            WHERE (
                location.name = v_start_location_name AND
                arrival_time >= v_start_arrival_time
            )
        ),
        end_point AS (
            SELECT
                point.id,
                point.thread_id,
                point.arrival_interval,
                trip.id AS trip_id,
                (trip.departure_time + point.arrival_interval) AS arrival_time
            FROM point
                INNER JOIN location ON (point.location_id = location.id)
                INNER JOIN thread ON (point.thread_id = thread.id)
                INNER JOIN trip ON (thread.id = trip.thread_id)
            WHERE location.name = v_end_location_name
        )
    SELECT
        start_point.thread_id,
        start_point.trip_id,
        start_point.id,
        start_point.arrival_interval,
        start_point.arrival_time,
        end_point.id,
        end_point.arrival_interval,
        end_point.arrival_time
    FROM start_point INNER JOIN end_point ON (start_point.trip_id = end_point.trip_id)
    WHERE start_point.arrival_interval <= end_point.arrival_interval
    ORDER BY start_point.arrival_time, start_point.thread_id, start_point.trip_id;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION reset_sequence(v_table_name text, v_column_name text DEFAULT 'id') RETURNS bigint AS $$
-- Устанавливает правильное значение счетчика заданной последовательности.
--
-- Параметры:
--     v_table_name: название таблицы.
--     v_column_name: название колонки.
--
-- Возвращаемое значение:
--     Новое значение счетчика.

DECLARE
    v_sequence_value bigint;

BEGIN
    EXECUTE
        format(
            $exec$
                SELECT setval(
                    pg_get_serial_sequence($1, $2),
                    coalesce(max(%I), 1),
                    max(%I) IS NOT NULL
                ) FROM %I;
            $exec$,
            v_column_name,
            v_column_name,
            v_table_name
        )
        INTO v_sequence_value
        USING v_table_name, v_column_name;

    RETURN v_sequence_value;
END;
$$ LANGUAGE plpgsql;
