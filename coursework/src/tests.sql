CREATE FUNCTION unit_tests.test_thread_update_columns() RETURNS test_result AS $$

DECLARE
    v_message test_result;
    v_result boolean;
    v_thread thread%ROWTYPE;

BEGIN
    BEGIN
        -- Создаем новое направление маршрута.
        INSERT INTO transport (id, name) VALUES
            (1, 'Автобус');

        INSERT INTO location (id, name) VALUES
            (1, 'Университет'),
            (2, 'Библиотека'),
            (3, 'Школа'),
            (4, 'Вокзал'),
            (5, 'Гостиница'),
            (6, 'Универмаг');

        INSERT INTO route (id, transport_id, number, created_at) VALUES
            (1, 1, '1', '2014-01-01');

        INSERT INTO thread (id, route_id) VALUES
            (1, 1);

        PERFORM reset_sequence(tables.name)
            FROM (VALUES
                ('transport'),
                ('location'),
                ('route'),
                ('thread')
            ) AS tables(name);

        -- Пункт отправления, пункт прибытия и время в пути должны быть NULL.
        SELECT * INTO v_thread FROM thread WHERE thread.id = 1;
        SELECT * FROM assert.is_equal(
            v_thread,
            ROW(1, 1, NULL, NULL, NULL)::thread
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        -- Добавляем несколько остановок.
        INSERT INTO point (id, thread_id, location_id, arrival_interval) VALUES
            (1, 1, 1, '00:00'),
            (2, 1, 2, '00:03'),
            (3, 1, 3, '00:08'),
            (4, 1, 4, '00:15'),
            (5, 1, 5, '00:20'),
            (6, 1, 6, '00:30');

        PERFORM reset_sequence('point');

        -- Пункт отправления, пункт прибытия и время в пути должны обновиться
        -- автоматически.
        SELECT * INTO v_thread FROM thread WHERE thread.id = 1;
        SELECT * FROM assert.is_equal(
            v_thread,
            ROW(1, 1, 1, 6, '00:30')::thread
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        -- Теперь попробуем обновить последнюю остановку.
        UPDATE point SET arrival_interval = '00:35' WHERE point.id = 6;

        -- Время в пути должно измениться.
        SELECT * INTO v_thread FROM thread WHERE thread.id = 1;
        SELECT * FROM assert.is_equal(
            v_thread,
            ROW(1, 1, 1, 6, '00:35')::thread
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        -- Удаляем несколько остановок.
        DELETE FROM point WHERE point.id IN (4, 5, 6);

        -- Должен обновиться пункт прибытия и время в пути.
        SELECT * INTO v_thread FROM thread WHERE thread.id = 1;
        SELECT * FROM assert.is_equal(
            v_thread,
            ROW(1, 1, 1, 3, '00:08')::thread
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        RAISE SQLSTATE 'PASS1';

    -- Таким способом мы откатываем все изменения, внесенные тестом в базу.
    -- Это нужно для того, чтобы юнит-тесты не могли влиять друг на друга.
    EXCEPTION
        -- Любой SQLSTATE должен состоять ровно из 5 символов, даже придуманный нами.
        WHEN SQLSTATE 'PASS1' THEN
            SELECT assert.ok('OK') INTO v_message;
            RETURN v_message;

        WHEN SQLSTATE 'FAIL1' THEN
            IF v_message IS NULL THEN
                SELECT assert.fail('FAIL') INTO v_message;
            END IF;
            RETURN v_message;
    END;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION unit_tests.test_thread_reverse() RETURNS test_result AS $$

DECLARE
    v_message test_result;
    v_result boolean;
    v_thread_id integer;
    v_thread thread%ROWTYPE;
    v_expected_results_count integer;
    v_actual_results_count integer;
    v_expected_point point%ROWTYPE;
    v_actual_point point%ROWTYPE;
    v_expected_trip trip%ROWTYPE;
    v_actual_trip trip%ROWTYPE;

BEGIN
    BEGIN
        INSERT INTO transport (id, name) VALUES
            (1, 'Автобус');

        INSERT INTO location (id, name) VALUES
            (1, 'Университет'),
            (2, 'Библиотека'),
            (3, 'Школа'),
            (4, 'Вокзал'),
            (5, 'Гостиница'),
            (6, 'Универмаг');

        INSERT INTO route (id, transport_id, number, created_at) VALUES
            (1, 1, '1', '2014-01-01');

        INSERT INTO thread (id, route_id) VALUES
            (1, 1);

        INSERT INTO point (id, thread_id, location_id, arrival_interval) VALUES
            (1, 1, 1, '00:00'),
            (2, 1, 2, '00:03'),
            (3, 1, 3, '00:08'),
            (4, 1, 4, '00:15'),
            (5, 1, 5, '00:20'),
            (6, 1, 6, '00:30');

        INSERT INTO trip (id, thread_id, departure_time) VALUES
            (1, 1, '12:00'),
            (2, 1, '13:10');

        PERFORM reset_sequence(tables.name)
            FROM (VALUES
                ('transport'),
                ('location'),
                ('route'),
                ('thread'),
                ('point'),
                ('trip')
            ) AS tables(name);

        SELECT thread_reverse(1, v_offset := '00:05') INTO v_thread_id;

        -- Проверяем, появилось ли в базе обратное направление маршрута.
        SELECT * INTO v_thread FROM thread WHERE thread.id = v_thread_id;
        SELECT * FROM assert.is_equal(
            v_thread,
            ROW(2, 1, 7, 12, '00:30')::thread
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        -- Проверяем, какие остановки с ним связаны.
        CREATE TEMP TABLE t_expected_results (
            LIKE point
        ) ON COMMIT DROP;
        INSERT INTO t_expected_results VALUES
             (7, 2, 6, '00:00'),
             (8, 2, 5, '00:10'),
             (9, 2, 4, '00:15'),
             (10, 2, 3, '00:22'),
             (11, 2, 2, '00:27'),
             (12, 2, 1, '00:30');

        -- FIXME: написать функцию, аналогичную results_eq() в pgTAP.
        SELECT COUNT(*) INTO v_expected_results_count FROM t_expected_results;
        SELECT COUNT(*) INTO v_actual_results_count FROM point WHERE point.thread_id = 2;
        SELECT * FROM assert.is_equal(v_expected_results_count, v_actual_results_count) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        FOR v_expected_point IN
            SELECT * FROM t_expected_results ORDER BY id
        LOOP
            SELECT * INTO v_actual_point FROM point WHERE point.id = v_expected_point.id;
            SELECT * FROM assert.is_equal(v_expected_point, v_actual_point) INTO v_message, v_result;
            IF NOT v_result THEN
                RAISE SQLSTATE 'FAIL1';
            END IF;
        END LOOP;

        -- Проверяем связанные обратные рейсы.
        DROP TABLE t_expected_results;
        CREATE TEMP TABLE t_expected_results (
            LIKE trip
        ) ON COMMIT DROP;
        INSERT INTO t_expected_results VALUES
            (3, 2, '12:35', '13:05'),
            (4, 2, '13:45', '14:15');

        SELECT COUNT(*) INTO v_expected_results_count FROM t_expected_results;
        SELECT COUNT(*) INTO v_actual_results_count FROM trip WHERE trip.thread_id = 2;
        SELECT * FROM assert.is_equal(v_expected_results_count, v_actual_results_count) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        FOR v_expected_trip IN
            SELECT * FROM t_expected_results ORDER BY id
        LOOP
            SELECT * INTO v_actual_trip FROM trip WHERE trip.id = v_expected_trip.id;
            SELECT * FROM assert.is_equal(v_expected_trip, v_actual_trip) INTO v_message, v_result;
            IF NOT v_result THEN
                RAISE SQLSTATE 'FAIL1';
            END IF;
        END LOOP;

        RAISE SQLSTATE 'PASS1';

    EXCEPTION
        WHEN SQLSTATE 'PASS1' THEN
            SELECT assert.ok('OK') INTO v_message;
            RETURN v_message;

        WHEN SQLSTATE 'FAIL1' THEN
            IF v_message IS NULL THEN
                SELECT assert.fail('FAIL') INTO v_message;
            END IF;
            RETURN v_message;
    END;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION unit_tests.test_trip_reverse() RETURNS test_result AS $$

DECLARE
    v_message test_result;
    v_result boolean;
    v_trip_id integer;
    v_trip trip%ROWTYPE;

BEGIN
    BEGIN
        INSERT INTO transport (id, name) VALUES
            (1, 'Автобус');

        INSERT INTO location (id, name) VALUES
            (1, 'Университет'),
            (2, 'Библиотека'),
            (3, 'Школа'),
            (4, 'Вокзал'),
            (5, 'Гостиница'),
            (6, 'Универмаг');

        INSERT INTO route (id, transport_id, number, created_at) VALUES
            (1, 1, '1', '2014-01-01');

        INSERT INTO thread (id, route_id) VALUES
            (1, 1);

        INSERT INTO point (id, thread_id, location_id, arrival_interval) VALUES
            (1, 1, 1, '00:00'),
            (2, 1, 2, '00:03'),
            (3, 1, 3, '00:08'),
            (4, 1, 4, '00:15'),
            (5, 1, 5, '00:20'),
            (6, 1, 6, '00:30');

        INSERT INTO trip (id, thread_id, departure_time) VALUES
            (1, 1, '12:00'),
            (2, 1, '13:10');

        PERFORM reset_sequence(tables.name)
            FROM (VALUES
                ('transport'),
                ('location'),
                ('route'),
                ('thread'),
                ('point'),
                ('trip')
            ) AS tables(name);

        PERFORM thread_reverse(1, v_copy_trips := FALSE);

        -- v_before = FALSE
        SELECT trip_reverse(1, 2, v_offset := '00:05') INTO v_trip_id;

        SELECT * INTO v_trip FROM trip WHERE trip.id = v_trip_id;
        SELECT * FROM assert.is_equal(
            v_trip,
            ROW(3, 2, '12:35', '13:05')::trip
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        -- v_before = TRUE
        SELECT trip_reverse(2, 2, v_offset := '00:00', v_before := TRUE) INTO v_trip_id;

        SELECT * INTO v_trip FROM trip WHERE trip.id = v_trip_id;
        SELECT * FROM assert.is_equal(
            v_trip,
            ROW(4, 2, '12:40', '13:10')::trip
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        RAISE SQLSTATE 'PASS1';

    EXCEPTION
        WHEN SQLSTATE 'PASS1' THEN
            SELECT assert.ok('OK') INTO v_message;
            RETURN v_message;

        WHEN SQLSTATE 'FAIL1' THEN
            IF v_message IS NULL THEN
                SELECT assert.fail('FAIL') INTO v_message;
            END IF;
            RETURN v_message;
    END;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION unit_tests.test_trip_before_save_row() RETURNS test_result AS $$

DECLARE
    v_message test_result;
    v_result boolean;
    v_trip trip%ROWTYPE;

BEGIN
    BEGIN
        INSERT INTO transport (id, name) VALUES
            (1, 'Автобус');

        INSERT INTO location (id, name) VALUES
            (1, 'Университет'),
            (2, 'Библиотека'),
            (3, 'Школа'),
            (4, 'Вокзал'),
            (5, 'Гостиница'),
            (6, 'Универмаг');

        INSERT INTO route (id, transport_id, number, created_at) VALUES
            (1, 1, '1', '2014-01-01');

        INSERT INTO thread (id, route_id) VALUES
            (1, 1);

        INSERT INTO point (id, thread_id, location_id, arrival_interval) VALUES
            (1, 1, 1, '00:00'),
            (2, 1, 2, '00:03'),
            (3, 1, 3, '00:08'),
            (4, 1, 4, '00:15'),
            (5, 1, 5, '00:20'),
            (6, 1, 6, '00:30');

        INSERT INTO trip (id, thread_id, departure_time) VALUES
            (1, 1, '12:00');

        PERFORM reset_sequence(tables.name)
            FROM (VALUES
                ('transport'),
                ('location'),
                ('route'),
                ('thread'),
                ('point'),
                ('trip')
            ) AS tables(name);

        SELECT * INTO v_trip FROM trip WHERE trip.id = 1;
        SELECT * FROM assert.is_equal(
            v_trip,
            ROW(1, 1, '12:00', '12:30')::trip
        ) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        RAISE SQLSTATE 'PASS1';

    EXCEPTION
        WHEN SQLSTATE 'PASS1' THEN
            SELECT assert.ok('OK') INTO v_message;
            RETURN v_message;

        WHEN SQLSTATE 'FAIL1' THEN
            IF v_message IS NULL THEN
                SELECT assert.fail('FAIL') INTO v_message;
            END IF;
            RETURN v_message;
    END;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION unit_tests.test_thread_search() RETURNS test_result AS $$

DECLARE
    v_message test_result;
    v_result boolean;
    v_expected_results_count integer;
    v_actual_results_count integer;
    v_expected_result RECORD;
    v_actual_result RECORD;

BEGIN
    BEGIN
        INSERT INTO transport (id, name) VALUES
            (1, 'Автобус'),
            (2, 'Троллейбус');

        INSERT INTO location (id, name) VALUES
            (1, 'Университет'),
            (2, 'Библиотека'),
            (3, 'Школа'),
            (4, 'Вокзал'),
            (5, 'Гостиница'),
            (6, 'Универмаг'),
            (7, 'Завод'),
            (8, 'Больница'),
            (9, 'Ресторан'),
            (10, 'Стадион');

        INSERT INTO route (id, transport_id, number, created_at) VALUES
            (1, 1, '1', '2014-01-01'),
            (2, 2, '1', '2014-01-01'),
            (3, 1, '2', DEFAULT);

        INSERT INTO thread (id, route_id) VALUES
            (1, 1),
            (2, 2),
            (3, 3);

        INSERT INTO point (id, thread_id, location_id, arrival_interval) VALUES
            (1, 1, 1, '00:00'),
            (2, 1, 2, '00:03'),
            (3, 1, 3, '00:08'),
            (4, 1, 4, '00:15'),
            (5, 1, 5, '00:20'),
            (6, 1, 6, '00:30'),

            (7, 2, 1, '00:00'),
            (8, 2, 2, '00:04'),
            (9, 2, 3, '00:10'),
            (10, 2, 4, '00:18'),
            (11, 2, 5, '00:25'),
            (12, 2, 6, '00:40'),

            (13, 3, 7, '00:00'),
            (14, 3, 8, '00:05'),
            (15, 3, 4, '00:11'),
            (16, 3, 9, '00:16'),
            (17, 3, 10, '00:20');

        INSERT INTO trip (id, thread_id, departure_time) VALUES
            (1, 1, '12:00'),
            (2, 1, '13:10'),
            (3, 2, '12:00'),
            (4, 2, '13:30'),
            (5, 3, '12:00'),
            (6, 3, '12:40');

        PERFORM reset_sequence(tables.name)
            FROM (VALUES
                ('transport'),
                ('location'),
                ('route'),
                ('thread'),
                ('point'),
                ('trip')
            ) AS tables(name);

        PERFORM
            thread_reverse(
                thread.id,
                v_offset := CASE
                    WHEN thread.id IN (1, 2) THEN interval '00:05'
                    WHEN thread.id = 3 THEN interval '00:00'
                END
            )
            FROM thread ORDER BY thread.id;

        -- v_start_arrival_time не задано
        CREATE TEMP TABLE t_actual_results ON COMMIT DROP AS
            SELECT * FROM thread_search('Университет', 'Вокзал') WITH ORDINALITY;

        CREATE TEMP TABLE t_expected_results (
            LIKE t_actual_results
        ) ON COMMIT DROP;
        INSERT INTO t_expected_results VALUES
            (1, 1, 1, '00:00', '12:00', 4,' 00:15', '12:15', 1),
            (2, 3, 7, '00:00', '12:00', 10, '00:18', '12:18', 2),
            (1, 2, 1, '00:00', '13:10', 4, '00:15', '13:25', 3),
            (2, 4, 7, '00:00', '13:30', 10, '00:18', '13:48', 4);

        SELECT COUNT(*) INTO v_expected_results_count FROM t_expected_results;
        SELECT COUNT(*) INTO v_actual_results_count FROM t_actual_results;
        SELECT * FROM assert.is_equal(v_expected_results_count, v_actual_results_count) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        FOR v_expected_result IN
            SELECT * FROM t_expected_results ORDER BY ordinality
        LOOP
            SELECT * INTO v_actual_result FROM t_actual_results
                WHERE t_actual_results.ordinality = v_expected_result.ordinality;
            SELECT * FROM assert.is_equal(v_expected_result::text, v_actual_result::text)
                INTO v_message, v_result;
            IF NOT v_result THEN
                RAISE SQLSTATE 'FAIL1';
            END IF;
        END LOOP;

        -- v_start_arrival_time = '13:00'
        DROP TABLE t_actual_results;
        CREATE TEMP TABLE t_actual_results ON COMMIT DROP AS
            SELECT * FROM thread_search('Университет', 'Вокзал', '13:00') WITH ORDINALITY;

        DROP TABLE t_expected_results;
        CREATE TEMP TABLE t_expected_results (
            LIKE t_actual_results
        ) ON COMMIT DROP;
        INSERT INTO t_expected_results VALUES
            (1, 2, 1, '00:00', '13:10', 4, '00:15', '13:25', 1),
            (2, 4, 7, '00:00', '13:30', 10, '00:18', '13:48', 2);

        SELECT COUNT(*) INTO v_expected_results_count FROM t_expected_results;
        SELECT COUNT(*) INTO v_actual_results_count FROM t_actual_results;
        SELECT * FROM assert.is_equal(v_expected_results_count, v_actual_results_count) INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        FOR v_expected_result IN
            SELECT * FROM t_expected_results ORDER BY ordinality
        LOOP
            SELECT * INTO v_actual_result FROM t_actual_results
                WHERE t_actual_results.ordinality = v_expected_result.ordinality;
            SELECT * FROM assert.is_equal(v_expected_result::text, v_actual_result::text)
                INTO v_message, v_result;
            IF NOT v_result THEN
                RAISE SQLSTATE 'FAIL1';
            END IF;
        END LOOP;

        RAISE SQLSTATE 'PASS1';

    EXCEPTION
        WHEN SQLSTATE 'PASS1' THEN
            SELECT assert.ok('OK') INTO v_message;
            RETURN v_message;

        WHEN SQLSTATE 'FAIL1' THEN
            IF v_message IS NULL THEN
                SELECT assert.fail('FAIL') INTO v_message;
            END IF;
            RETURN v_message;
    END;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION unit_tests.test_reset_sequence() RETURNS test_result AS $$

DECLARE
    v_message test_result;
    v_result boolean;
    v_sequence_value bigint;

BEGIN
    BEGIN
        INSERT INTO location (id, name) VALUES
            (1, 'Университет'),
            (2, 'Библиотека'),
            (3, 'Школа'),
            (4, 'Вокзал'),
            (5, 'Гостиница'),
            (6, 'Универмаг'),
            (7, 'Завод'),
            (8, 'Больница'),
            (9, 'Ресторан'),
            (10, 'Стадион');

        SELECT reset_sequence('location') INTO v_sequence_value;

        SELECT * FROM assert.is_equal(
            v_sequence_value,
            currval(pg_get_serial_sequence('location', 'id'))
        )
            INTO v_message, v_result;
        IF NOT v_result THEN
            RAISE SQLSTATE 'FAIL1';
        END IF;

        RAISE SQLSTATE 'PASS1';

    EXCEPTION
        WHEN SQLSTATE 'PASS1' THEN
            SELECT assert.ok('OK') INTO v_message;
            RETURN v_message;

        WHEN SQLSTATE 'FAIL1' THEN
            IF v_message IS NULL THEN
                SELECT assert.fail('FAIL') INTO v_message;
            END IF;
            RETURN v_message;
    END;
END;
$$ LANGUAGE plpgsql;


/*
CREATE FUNCTION unit_tests.test_function_template() RETURNS test_result AS $$
-- Заготовка для юнит-теста.

DECLARE
    v_message test_result;
    v_result boolean;

BEGIN
    BEGIN
        RAISE SQLSTATE 'PASS1';

    EXCEPTION
        WHEN SQLSTATE 'PASS1' THEN
            SELECT assert.ok('OK') INTO v_message;
            RETURN v_message;

        WHEN SQLSTATE 'FAIL1' THEN
            IF v_message IS NULL THEN
                SELECT assert.fail('FAIL') INTO v_message;
            END IF;
            RETURN v_message;
    END;
END;
$$ LANGUAGE plpgsql;
*/
