==================================================
Курсовая работа по дисциплине «Управление данными»
==================================================

Тема курсовой работы
====================

**ИС: «Городской транспорт».** Минимальный список характеристик: вид
транспорта, номер  маршрута, дата введения маршрута, начальная остановка,
конечная остановка, время в пути.

Системные требования
====================

* PostgreSQL 9.x
* plpgunit (для тестов)

Установка
=========

В ходе установки, помимо таблиц и функций, в базу будет добавлено немного
тестовых данных::

    $ createdb transport_pro
    $ psql -f coursework/src/install.sql transport_pro

Запуск тестов
=============

Для запуска тестов нам понадобится чистая база данных. Кроме этого, нужно будет
установить в неё `plpgunit <https://github.com/mixerp/plpgunit>`_::

    $ createdb transport_test
    $ psql -f plpgunit/install/install-unit-test.sql transport_test

А теперь запускаем тесты::

    $ psql -f coursework/src/run_tests.sql transport_test

TODO
====

* диаграммы
* пояснительная записка
