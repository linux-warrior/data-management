from django.contrib import admin
from transport.models import Transport, Location, Route, Thread, Point, Trip


admin.site.register([Transport, Location, Route, Thread, Point, Trip])
