# -*- coding: utf-8 -*-
import datetime

from django.db import models
from interval.fields import IntervalField


class Transport(models.Model):
    name = models.CharField(verbose_name=u'название', max_length=256, unique=True)
    description = models.TextField(verbose_name=u'описание')

    class Meta:
        verbose_name = u'вид транспорта'
        verbose_name_plural = u'виды транспорта'
        db_table = 'transport'

    def __unicode__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(verbose_name=u'название', max_length=256, unique=True)
    description = models.TextField(verbose_name=u'описание')

    class Meta:
        verbose_name = u'станция'
        verbose_name_plural = u'станции'
        db_table = 'location'

    def __unicode__(self):
        return self.name


class Route(models.Model):
    transport = models.ForeignKey('Transport', verbose_name=u'вид транспорта')
    number = models.CharField(verbose_name=u'номер маршрута', max_length=16, db_index=True)
    description = models.TextField(verbose_name=u'описание')
    created_at = models.DateField(verbose_name=u'дата введения', default=datetime.date.today)

    class Meta:
        verbose_name = u'маршрут'
        verbose_name_plural = u'маршруты'
        db_table = 'route'
        unique_together = ('transport', 'number')

    def __unicode__(self):
        return u'{transport} #{number}'.format(
            transport=self.transport,
            number=self.number,
        )


class Thread(models.Model):
    route = models.ForeignKey('Route', verbose_name=u'маршрут')
    points = models.ManyToManyField('Location', through='Point', verbose_name=u'остановки')
    departure_point = models.ForeignKey(
        'Point',
        verbose_name=u'пункт отправления',
        related_name='+',
        null=True,
        on_delete=models.SET_NULL,
    )
    arrival_point = models.ForeignKey(
        'Point',
        verbose_name=u'пункт прибытия',
        related_name='+',
        null=True,
        on_delete=models.SET_NULL,
    )
    trip_duration = IntervalField(verbose_name=u'время в пути', null=True)

    class Meta:
        verbose_name = u'направление маршрута'
        verbose_name_plural = u'направления маршрутов'
        db_table = 'thread'

    def __unicode__(self):
        return u'{route} ({departure_point} - {arrival_point})'.format(
            route=self.route,
            departure_point=self.departure_point,
            arrival_point=self.arrival_point,
        )


class Point(models.Model):
    thread = models.ForeignKey('Thread', verbose_name=u'направление маршрута')
    location = models.ForeignKey('Location', verbose_name=u'станция')
    arrival_interval = IntervalField(verbose_name=u'интервал прибытия', db_index=True)

    class Meta:
        verbose_name = u'остановка'
        verbose_name_plural = u'остановки'
        db_table = 'point'
        unique_together = ('thread', 'arrival_interval')

    def __unicode__(self):
        return u'{thread} / {location} ({arrival_interval})'.format(
            thread=self.thread,
            location=self.location,
            arrival_interval=self.arrival_interval,
        )


class Trip(models.Model):
    thread = models.ForeignKey('Thread', verbose_name=u'направление маршрута')
    departure_time = models.TimeField(verbose_name=u'время отправления', db_index=True)
    arrival_time = models.TimeField(verbose_name=u'время прибытия', db_index=True)

    class Meta:
        verbose_name = u'рейс'
        verbose_name_plural = u'рейсы'
        db_table = 'trip'

    def __unicode__(self):
        return u'{thread} ({departure_time} - {arrival_time})'.format(
            thread=self.thread,
            departure_time=self.departure_time,
            arrival_time=self.arrival_time,
        )
