# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import interval.fields
import datetime
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=256, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'location',
                'verbose_name': '\u0441\u0442\u0430\u043d\u0446\u0438\u044f',
                'verbose_name_plural': '\u0441\u0442\u0430\u043d\u0446\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('arrival_interval', interval.fields.IntervalField(verbose_name='\u0438\u043d\u0442\u0435\u0440\u0432\u0430\u043b \u043f\u0440\u0438\u0431\u044b\u0442\u0438\u044f', db_index=True)),
                ('location', models.ForeignKey(verbose_name='\u0441\u0442\u0430\u043d\u0446\u0438\u044f', to='transport.Location')),
            ],
            options={
                'db_table': 'point',
                'verbose_name': '\u043e\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0430',
                'verbose_name_plural': '\u043e\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=16, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430', db_index=True)),
                ('description', models.TextField(verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('created_at', models.DateField(default=datetime.date.today, verbose_name='\u0434\u0430\u0442\u0430 \u0432\u0432\u0435\u0434\u0435\u043d\u0438\u044f')),
            ],
            options={
                'db_table': 'route',
                'verbose_name': '\u043c\u0430\u0440\u0448\u0440\u0443\u0442',
                'verbose_name_plural': '\u043c\u0430\u0440\u0448\u0440\u0443\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('trip_duration', interval.fields.IntervalField(null=True, verbose_name='\u0432\u0440\u0435\u043c\u044f \u0432 \u043f\u0443\u0442\u0438')),
                ('arrival_point', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='\u043f\u0443\u043d\u043a\u0442 \u043f\u0440\u0438\u0431\u044b\u0442\u0438\u044f', to='transport.Point', null=True)),
                ('departure_point', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='\u043f\u0443\u043d\u043a\u0442 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f', to='transport.Point', null=True)),
                ('points', models.ManyToManyField(to='transport.Location', verbose_name='\u043e\u0441\u0442\u0430\u043d\u043e\u0432\u043a\u0438', through='transport.Point')),
                ('route', models.ForeignKey(verbose_name='\u043c\u0430\u0440\u0448\u0440\u0443\u0442', to='transport.Route')),
            ],
            options={
                'db_table': 'thread',
                'verbose_name': '\u043d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430',
                'verbose_name_plural': '\u043d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Transport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=256, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'transport',
                'verbose_name': '\u0432\u0438\u0434 \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0430',
                'verbose_name_plural': '\u0432\u0438\u0434\u044b \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Trip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('departure_time', models.TimeField(verbose_name='\u0432\u0440\u0435\u043c\u044f \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f', db_index=True)),
                ('arrival_time', models.TimeField(verbose_name='\u0432\u0440\u0435\u043c\u044f \u043f\u0440\u0438\u0431\u044b\u0442\u0438\u044f', db_index=True)),
                ('thread', models.ForeignKey(verbose_name='\u043d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430', to='transport.Thread')),
            ],
            options={
                'db_table': 'trip',
                'verbose_name': '\u0440\u0435\u0439\u0441',
                'verbose_name_plural': '\u0440\u0435\u0439\u0441\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='route',
            name='transport',
            field=models.ForeignKey(verbose_name='\u0432\u0438\u0434 \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u0430', to='transport.Transport'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='route',
            unique_together=set([('transport', 'number')]),
        ),
        migrations.AddField(
            model_name='point',
            name='thread',
            field=models.ForeignKey(verbose_name='\u043d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430', to='transport.Thread'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='point',
            unique_together=set([('thread', 'arrival_interval')]),
        ),
    ]
