-- Виды транспорта.
CREATE TABLE "transport" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(256) NOT NULL UNIQUE,  -- название
    "description" text NOT NULL DEFAULT ''  -- описание
);


-- Маршруты.
CREATE TABLE "route" (
    "id" serial NOT NULL PRIMARY KEY,
    "transport_id" integer NOT NULL DEFAULT 1,  -- вид транспорта
    "number" varchar(16) NOT NULL,  -- номер маршрута
    "description" text NOT NULL DEFAULT '',  -- описание
    "created_at" date NOT NULL,  -- дата введения
    UNIQUE ("transport_id", "number")
);
