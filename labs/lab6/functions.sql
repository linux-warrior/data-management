CREATE FUNCTION transport_after_update_row() RETURNS trigger AS $$
-- ON UPDATE SET DEFAULT

BEGIN
    UPDATE route SET transport_id = DEFAULT
        WHERE route.transport_id = OLD.id;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION transport_after_delete_row() RETURNS trigger AS $$
-- ON DELETE CASCADE

BEGIN
    DELETE FROM route
        WHERE route.transport_id = OLD.id;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER transport_after_update_row AFTER UPDATE ON transport
    FOR EACH ROW EXECUTE PROCEDURE transport_after_update_row();
CREATE TRIGGER transport_after_delete_row AFTER DELETE ON transport
    FOR EACH ROW EXECUTE PROCEDURE transport_after_delete_row();


CREATE FUNCTION route_before_save_row() RETURNS trigger AS $$
-- Триггер бизнес-логики.

BEGIN
    IF NEW.created_at IS NULL THEN
        NEW.created_at := CURRENT_DATE;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER route_before_save_row BEFORE INSERT OR UPDATE ON route
    FOR EACH ROW EXECUTE PROCEDURE route_before_save_row();
