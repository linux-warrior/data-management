BEGIN;

\ir ../common/utils.sql
\ir tables.sql
\ir functions.sql

INSERT INTO transport (id, name) VALUES
    (1, '<По умолчанию>'),
    (2, 'Автобус'),
    (3, 'Троллейбус');

INSERT INTO route (id, transport_id, number) VALUES
    (1, 2, '1'),
    (2, 2, '2'),
    (3, 3, '1'),
    (4, 3, '2');

SELECT reset_sequence(tables.name)
    FROM (VALUES
        ('transport'),
        ('route')
    ) AS tables(name);

DO $$ BEGIN RAISE NOTICE 'transport:'; END; $$;
SELECT * FROM transport ORDER BY id;

DO $$ BEGIN RAISE NOTICE 'route:'; END; $$;
SELECT * FROM route ORDER BY id;

UPDATE transport SET id = 1000 WHERE transport.id = 2;

DO $$ BEGIN RAISE NOTICE 'transport:'; END; $$;
SELECT * FROM transport ORDER BY id;

DO $$ BEGIN RAISE NOTICE 'route:'; END; $$;
SELECT * FROM route ORDER BY id;

DELETE FROM transport WHERE transport.id = 3;

DO $$ BEGIN RAISE NOTICE 'transport:'; END; $$;
SELECT * FROM transport ORDER BY id;

DO $$ BEGIN RAISE NOTICE 'route:'; END; $$;
SELECT * FROM route ORDER BY id;

ROLLBACK;
