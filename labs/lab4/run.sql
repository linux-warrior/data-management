BEGIN;

\ir ../common/tables.sql
\ir ../common/functions.sql
\ir ../common/fixtures.sql
\ir views.sql

DO $$ BEGIN RAISE NOTICE 'transport:'; END; $$;
SELECT * FROM transport ORDER BY id;

DO $$ BEGIN RAISE NOTICE 'route:'; END; $$;
SELECT * FROM route ORDER BY id;

DO $$ BEGIN
    RAISE NOTICE 'Примеры операций над обновляемым представлением.';
    RAISE NOTICE 'bus_routes:';
END; $$;
SELECT * FROM bus_routes ORDER BY id;

INSERT INTO bus_routes (transport_id, number, created_at) VALUES
    (1, '3', DEFAULT);

DO $$ BEGIN RAISE NOTICE 'bus_routes:'; END; $$;
SELECT * FROM bus_routes ORDER BY id;

DO $$ BEGIN RAISE NOTICE 'route:'; END; $$;
SELECT * FROM route ORDER BY id;

UPDATE bus_routes SET
    number = '3a'
    WHERE bus_routes.number = '3';

DO $$ BEGIN RAISE NOTICE 'bus_routes:'; END; $$;
SELECT * FROM bus_routes ORDER BY id;

DO $$ BEGIN RAISE NOTICE 'route:'; END; $$;
SELECT * FROM route ORDER BY id;

DELETE FROM bus_routes
    WHERE bus_routes.number = '3a';

DO $$ BEGIN RAISE NOTICE 'bus_routes:'; END; $$;
SELECT * FROM bus_routes ORDER BY id;

DO $$ BEGIN RAISE NOTICE 'route:'; END; $$;
SELECT * FROM route ORDER BY id;

DO $$ BEGIN
    RAISE NOTICE 'Пример выборки из необновляемого представления.';
    RAISE NOTICE 'transport_routes:';
END; $$;
SELECT * FROM transport_routes WHERE lower(transport_routes.name) = 'автобус';

ROLLBACK;
