-- Это обновляемое представление.
CREATE VIEW bus_routes AS
    SELECT * FROM route WHERE route.transport_id IN (
        SELECT transport.id FROM transport
        WHERE lower(transport.name) = 'автобус'
    )
    WITH LOCAL CHECK OPTION;


-- Это необновляемое представление, поскольку оно содержит
-- более одной таблицы во FROM clause.
CREATE VIEW transport_routes AS
    SELECT transport.name, route.number, route.created_at
    FROM transport
        LEFT JOIN route ON (transport.id = route.transport_id)
    ORDER BY transport.name, route.number;
