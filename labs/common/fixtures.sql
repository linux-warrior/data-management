INSERT INTO transport (id, name) VALUES
    (1, 'Автобус'),
    (2, 'Троллейбус');

INSERT INTO location (id, name) VALUES
    (1, 'Университет'),
    (2, 'Библиотека'),
    (3, 'Школа'),
    (4, 'Вокзал'),
    (5, 'Гостиница'),
    (6, 'Универмаг'),
    (7, 'Завод'),
    (8, 'Больница'),
    (9, 'Ресторан'),
    (10, 'Стадион');

INSERT INTO route (id, transport_id, number, created_at) VALUES
    (1, 1, '1', '2014-01-01'),
    (2, 2, '1', '2014-01-01'),
    (3, 1, '2', DEFAULT);

INSERT INTO thread (id, route_id) VALUES
    (1, 1),
    (2, 2),
    (3, 3);

INSERT INTO point (id, thread_id, location_id, arrival_interval) VALUES
    (1, 1, 1, '00:00'),
    (2, 1, 2, '00:03'),
    (3, 1, 3, '00:08'),
    (4, 1, 4, '00:15'),
    (5, 1, 5, '00:20'),
    (6, 1, 6, '00:30'),

    (7, 2, 1, '00:00'),
    (8, 2, 2, '00:04'),
    (9, 2, 3, '00:10'),
    (10, 2, 4, '00:18'),
    (11, 2, 5, '00:25'),
    (12, 2, 6, '00:40'),

    (13, 3, 7, '00:00'),
    (14, 3, 8, '00:05'),
    (15, 3, 4, '00:11'),
    (16, 3, 9, '00:16'),
    (17, 3, 10, '00:20');

INSERT INTO trip (id, thread_id, departure_time) VALUES
    (1, 1, '12:00'),
    (2, 1, '13:10'),
    (3, 2, '12:00'),
    (4, 2, '13:30'),
    (5, 3, '12:00'),
    (6, 3, '12:40');

SELECT reset_sequence(tables.name)
    FROM (VALUES
        ('transport'),
        ('location'),
        ('route'),
        ('thread'),
        ('point'),
        ('trip')
    ) AS tables(name);

SELECT
    thread_reverse(
        thread.id,
        v_offset := CASE
            WHEN thread.id IN (1, 2) THEN interval '00:05'
            WHEN thread.id = 3 THEN interval '00:00'
        END
    )
    FROM thread ORDER BY thread.id;
