CREATE FUNCTION reset_sequence(v_table_name text,
                               v_column_name text DEFAULT 'id')
    RETURNS bigint AS $$
-- Устанавливает правильное значение счетчика заданной последовательности.
--
-- Параметры:
--     v_table_name: название таблицы.
--     v_column_name: название колонки.
--
-- Возвращаемое значение:
--     Новое значение счетчика.

DECLARE
    v_sequence_value bigint;

BEGIN
    EXECUTE
        format(
            $exec$
                SELECT setval(
                    pg_get_serial_sequence($1, $2),
                    coalesce(max(%I), 1),
                    max(%I) IS NOT NULL
                ) FROM %I;
            $exec$,
            v_column_name,
            v_column_name,
            v_table_name
        )
        INTO v_sequence_value
        USING v_table_name, v_column_name;

    RETURN v_sequence_value;
END;
$$ LANGUAGE plpgsql;
