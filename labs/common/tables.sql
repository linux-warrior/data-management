-- Виды транспорта.
CREATE TABLE "transport" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(256) NOT NULL UNIQUE,  -- название
    "description" text NOT NULL DEFAULT ''  -- описание
);


-- Станции.
CREATE TABLE "location" (
    "id" serial NOT NULL PRIMARY KEY,
    "name" varchar(256) NOT NULL UNIQUE,  -- название
    "description" text NOT NULL DEFAULT ''  -- описание
);


-- Маршруты.
CREATE TABLE "route" (
    "id" serial NOT NULL PRIMARY KEY,
    "transport_id" integer NOT NULL
        REFERENCES "transport" ("id") DEFERRABLE,  -- вид транспорта
    "number" varchar(16) NOT NULL,  -- номер маршрута
    "description" text NOT NULL DEFAULT '',  -- описание
    "created_at" date NOT NULL DEFAULT CURRENT_DATE,  -- дата введения
    UNIQUE ("transport_id", "number")
);


-- Направления маршрутов.
--
-- Обычно у каждого маршрута есть два направления - прямое и обратное.
CREATE TABLE "thread" (
    "id" serial NOT NULL PRIMARY KEY,
    "route_id" integer NOT NULL
        REFERENCES "route" ("id") DEFERRABLE,  -- маршрут
    "departure_point_id" integer,  -- пункт отправления
    "arrival_point_id" integer,  -- пункт прибытия
    "trip_duration" interval CHECK ("trip_duration" >= '0')  -- время в пути
);


-- Остановки.
CREATE TABLE "point" (
    "id" serial NOT NULL PRIMARY KEY,
    "thread_id" integer NOT NULL
        REFERENCES "thread" ("id") DEFERRABLE,  -- направление маршрута
    "location_id" integer NOT NULL
        REFERENCES "location" ("id") DEFERRABLE,  -- станция
    "arrival_interval" interval NOT NULL
        CHECK ("arrival_interval" >= '0'),  -- интервал прибытия
    UNIQUE ("thread_id", "arrival_interval")
);


ALTER TABLE "thread" ADD CONSTRAINT "departure_point_id_refs_id"
    FOREIGN KEY ("departure_point_id") REFERENCES "point" ("id")
    ON DELETE SET NULL;
ALTER TABLE "thread" ADD CONSTRAINT "arrival_point_id_refs_id"
    FOREIGN KEY ("arrival_point_id") REFERENCES "point" ("id")
    ON DELETE SET NULL;


-- Рейсы.
CREATE TABLE "trip" (
    "id" serial NOT NULL PRIMARY KEY,
    "thread_id" integer NOT NULL
        REFERENCES "thread" ("id") DEFERRABLE,  -- направление маршрута
    "departure_time" time NOT NULL,  -- время отправления
    "arrival_time" time NOT NULL  -- время прибытия
);


CREATE INDEX "transport_name_like" ON "transport" ("name" varchar_pattern_ops);

CREATE INDEX "location_name_like" ON "location" ("name" varchar_pattern_ops);

CREATE INDEX "route_transport_id" ON "route" ("transport_id");
CREATE INDEX "route_number" ON "route" ("number");
CREATE INDEX "route_number_like" ON "route" ("number" varchar_pattern_ops);

CREATE INDEX "thread_route_id" ON "thread" ("route_id");
CREATE INDEX "thread_departure_point_id" ON "thread" ("departure_point_id");
CREATE INDEX "thread_arrival_point_id" ON "thread" ("arrival_point_id");

CREATE INDEX "point_thread_id" ON "point" ("thread_id");
CREATE INDEX "point_location_id" ON "point" ("location_id");
CREATE INDEX "point_arrival_interval" ON "point" ("arrival_interval");

CREATE INDEX "trip_thread_id" ON "trip" ("thread_id");
CREATE INDEX "trip_departure_time" ON "trip" ("departure_time");
CREATE INDEX "trip_arrival_time" ON "trip" ("arrival_time");
