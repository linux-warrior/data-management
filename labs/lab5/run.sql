BEGIN;

\ir ../common/tables.sql
\ir ../common/functions.sql
\ir ../common/fixtures.sql
\ir ../lab4/views.sql
\ir functions.sql

SELECT *
    FROM get_routes_by_transport_name('авто%') AS found_routes
    ORDER BY found_routes.name, found_routes.number;

ROLLBACK;
