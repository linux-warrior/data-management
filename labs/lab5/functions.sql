-- Примеры функций можно посмотреть в файле ../common/functions.sql, а примеры
-- их вызова - в файле ../common/fixtures.sql. Тем не менее, для выполнения
-- формальных требований к этой лабораторной работе мы придумаем еще одну
-- несложную функцию, использующую ранее созданное представление.

CREATE FUNCTION get_routes_by_transport_name(v_name_like text)
    RETURNS TABLE (
        name text,
        number text,
        creation_year integer
    ) AS $$

BEGIN
    RETURN QUERY
        SELECT
            transport_routes.name::text,
            transport_routes.number::text,
            EXTRACT(
                YEAR FROM transport_routes.created_at
            )::integer AS creation_year
        FROM transport_routes
        WHERE lower(transport_routes.name) LIKE lower(v_name_like);
END;
$$ LANGUAGE plpgsql;
