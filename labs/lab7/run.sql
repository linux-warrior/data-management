BEGIN;

CREATE TABLE employee (
    id serial NOT NULL PRIMARY KEY,
    name varchar(256) NOT NULL,
    position varchar(256) NOT NULL,
    salary integer NOT NULL
);


CREATE FUNCTION raise_salaries_without_cursor() RETURNS void AS $$

DECLARE
    v_employee RECORD;

BEGIN
    FOR v_employee IN
        SELECT * FROM employee ORDER BY employee.id
    LOOP
        IF v_employee.position = 'программист' THEN
            UPDATE employee SET salary = salary * 2
                WHERE employee.id = v_employee.id;
        END IF;
    END LOOP;
END;
$$ LANGUAGE plpgsql;


CREATE FUNCTION raise_salaries_with_cursor() RETURNS void AS $$

DECLARE
    v_employee_cur refcursor;
    v_employee RECORD;

BEGIN
    OPEN v_employee_cur FOR SELECT * FROM employee ORDER BY employee.id;
    LOOP
        FETCH v_employee_cur INTO v_employee;
        EXIT WHEN NOT FOUND;
        IF v_employee.position = 'программист' THEN
            UPDATE employee SET salary = salary * 2
                WHERE employee.id = v_employee.id;
        END IF;
    END LOOP;
    CLOSE v_employee_cur;
END;
$$ LANGUAGE plpgsql;


INSERT INTO employee (name, position, salary) VALUES
    ('Иванов', 'программист', 30000),
    ('Петров', 'программист', 40000),
    ('Сидоров', 'менеджер', 35000);

DO $$ BEGIN RAISE NOTICE 'employee:'; END; $$;
SELECT * FROM employee ORDER BY id;

SAVEPOINT before_raise;

SELECT raise_salaries_without_cursor();

DO $$ BEGIN RAISE NOTICE 'employee:'; END; $$;
SELECT * FROM employee ORDER BY id;

ROLLBACK TO SAVEPOINT before_raise;

SELECT raise_salaries_with_cursor();

DO $$ BEGIN RAISE NOTICE 'employee:'; END; $$;
SELECT * FROM employee ORDER BY id;

ROLLBACK;
