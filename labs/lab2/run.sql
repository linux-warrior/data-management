BEGIN;

\ir ../common/tables.sql
\ir ../common/functions.sql
\ir ../common/fixtures.sql

-- Пусть вместо троллейбусов будут трамваи...
UPDATE transport SET
    name = 'Трамвай'
    WHERE transport.name = 'Троллейбус';

-- Смещаем все трамвайные рейсы на 1 час вперед.
UPDATE trip SET
    departure_time = departure_time + '01:00'
    WHERE trip.id IN (
        SELECT trip.id
        FROM transport
            INNER JOIN route ON (transport.id = route.transport_id)
            INNER JOIN thread ON (route.id = thread.route_id)
            INNER JOIN trip ON (thread.id = trip.thread_id)
        WHERE transport.name = 'Трамвай'
    );

-- Удаляем все, что связано с автобусами, эмулируя каскадное удаление.
WITH
    deleted_transports AS (
        DELETE FROM transport
            WHERE transport.name = 'Автобус'
            RETURNING transport.id
    ),
    deleted_routes AS (
        DELETE FROM route
            WHERE route.transport_id IN (SELECT * FROM deleted_transports)
            RETURNING route.id
    ),
    deleted_threads AS (
        DELETE FROM thread
            WHERE thread.route_id IN (SELECT * FROM deleted_routes)
            RETURNING thread.id
    ),
    deleted_points AS (
        DELETE FROM point
            WHERE point.thread_id IN (SELECT * FROM deleted_threads)
            RETURNING point.id
    ),
    deleted_trips AS (
        DELETE FROM trip
            WHERE trip.thread_id IN (SELECT * FROM deleted_threads)
            RETURNING trip.id
    )
SELECT * FROM deleted_transports ORDER BY id;

ROLLBACK;
