BEGIN;

\ir ../common/tables.sql
\ir ../common/functions.sql
\ir ../common/fixtures.sql

-- Выводим виды транспорта, для которых создано более одного маршрута.
SELECT transport.name, count(route.id) AS num_routes
FROM transport
    INNER JOIN route ON (transport.id = route.transport_id)
GROUP BY transport.name
HAVING count(route.id) > 1
ORDER BY transport.name;

-- Выводим максимальное и минимальное время прибытия для рейсов, относящихся
-- к видам транспорта, названия которых начинаются на "авто-" и время
-- отправления которых находится в промежутке между 12:00 и 13:00.
SELECT
    min(trip.arrival_time) AS min_arrival_time,
    max(trip.arrival_time) AS max_arrival_time
FROM trip
WHERE (
    trip.thread_id IN (
        SELECT thread.id
        FROM thread
            INNER JOIN (
                SELECT route.id
                FROM route
                    INNER JOIN transport ON (route.transport_id = transport.id)
                WHERE lower(transport.name) LIKE 'авто%'
            ) AS auto_routes ON (thread.route_id = auto_routes.id)
     ) AND
     trip.departure_time BETWEEN '12:00' AND '13:00'
);

ROLLBACK;
